//
//  ViewController.swift
//  showTableView
//
//  Created by OSX on 14/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var showRecords:NSArray = [];
    
    override func viewDidLoad() {
        super.viewDidLoad()

        showRecords = ["Vijay", "Pooja", "Tamanna", "Shiv", "Akshda", "Damini", "Garima", "Koshal"];
        
        let showTV:UITableView = UITableView()
        
        showTV.frame = self.view.frame
        
        showTV.delegate = self
        showTV.dataSource = self
        
        // If user want to increase tableview height

        showTV.rowHeight = 70
        
        self.view.addSubview(showTV)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return showRecords.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellIdentifier:NSString = "recordTVCell"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String) ?? UITableViewCell(style: .Default, reuseIdentifier: cellIdentifier as String)
        
        cell.textLabel?.text = showRecords.objectAtIndex(indexPath.row) as? String
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let controller:UIAlertController = UIAlertController(title: "Row selected", message: NSString(format: "You've selected a %ld row", indexPath.row) as String , preferredStyle: .Alert)
        
        let okBtnAction = UIAlertAction(title: "OK", style: .Default) { (UIAlertAction) in
            let cell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
        
        controller.addAction(okBtnAction)
        self.presentViewController(controller, animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

